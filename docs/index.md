# Herzlich willkommen!

Hier entsteht ein Wiki für OpenLP-Anwender im deutschsprachigen Raum.
Dieses Wiki soll den deutschsprachigen Anwendern von OpenLP den Umstieg oder den Einsatz von [OpenLP](http://www.openlp.org) erleichtern.
Wir wollen sowohl das Basiswissen schnell verfügbar machen als auch weiterführende Konfigurationsmöglichkeiten erläutern.

## Links zu den offiziellen OpenLP-Seiten:

- [Handbuch (Englisch)](http://manual.openlp.org/)
- [Wiki (Englisch)](http://wiki.openlp.org/)
- [E-Mail](mailto:support@openlp.org)
- IRC Channel: #openlp auf Freenode.net
- Developer Central: [http://openlp.io](http://openlp.io/)